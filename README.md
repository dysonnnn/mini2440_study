# mini2440_study

#### 介绍
mini2440开发板学习

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx 
3.  xxxx

#### 使用说明

1.  xxxxzxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)



操作步骤：
软件环境： windows10 WSL ubuntu 18.94
1. 搭建交叉编译环境
```sh
cd ~/
# 权限
chmod 777 arm-linux-gcc-4.4.3.tar.gz

# 解压交叉编译工具
tar xzf arm-linux-gcc-4.4.3.tar.gz

#拷贝工具到指定路径
sudo cp ~/opt/ / -rf  

# 添加环境变量
echo "export PATH=\$PATH:/opt/FriendlyARM/toolschain/4.4.3/bin" >> ~/.zshrc
source ~/.zshrc


# 检查arm-linux-gcc -v 版本号
arm-linux-gcc -v
/opt/FriendlyARM/toolschain/4.4.3/bin/arm-linux-gcc: 15: exec: /opt/FriendlyARM/toolschain/4.4.3/bin/.arm-none-linux-gnueabi-gcc: Exec format error      

# 解决方法：
# 工具是32位的，系统是64位的，需要安装32位的库
sudo apt install lib32ncurses5 lib32z1

# 报错：

The following packages have unmet dependencies:
 lib32ncurses5 : Depends: lib32tinfo5 (= 6.0+20160213-1ubuntu1) but it is not going to be installed
                 Depends: libc6-i386 (>= 2.4) but it is not going to be installed
 lib32z1 : Depends: libc6-i386 (>= 2.4) but it is not going to be installed
E: Unable to correct problems, you have held broken packages.

# 覆盖安装，参考 https://blog.csdn.net/duanlove/article/details/54666441
sudo apt-get install lib32tinfo5=6.0+20160213-1ubuntu1
sudo apt-get install libc6-i386= 2.4
sudo apt-get install libc6-i386=2.16


$ sudo apt list | grep libc6-i386
WARNING: apt does not have a stable CLI interface. Use with caution in scripts.
libc6-i386/xenial-security,xenial-updates 2.23-0ubuntu11.3 amd64

# 版本对不上，换源，参考 https://blog.csdn.net/qq_35451572/article/details/79516563
# 理论上，替换阿里云服务器上的source.list也是可以的




# 安装32位相关工具
sudo apt install lib32ncurses5 lib32z1

# 编译的时候出错：
/opt/FriendlyARM/toolschain/4.4.3/libexec/gcc/arm-none-linux-gnueabi/4.4.3/cc1: error while loading shared libraries: libstdc++.so.6: cannot open shared object file: No such file or directory

# 安装
sudo apt-get install lib32stdc++6

# 安装成功

# **貌似WSL不支持arm-linux-gcc......**


```



```sh
# 默认注释了源码镜像以提高 apt update 速度，如有需要可自行取消注释
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-updates main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-updates main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-backports main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-backports main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-security main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-security main restricted universe multiverse

# 预发布软件源，不建议启用
# deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-proposed main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-proposed main restricted universe multiverse
#————————————————
#版权声明：本文为CSDN博主「泉伟」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
#原文链接：https://blog.csdn.net/qq_35451572/article/details/79516563
```




2. 


